<?php

namespace App\Form;

use App\Entity\Comment;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class CommentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
            ->add('content')
            // ->add('date', DateType::class, [
            //     // 'widget' => 'single_text',
            //     'required' => false,
            //     'data' => new \DateTime("now"),
            //     'format' => 'd M y',
            //     'html5' => false
            //     ])
    
            // ->add('post')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Comment::class,
            'csrf_protection'=>false
        ]);
    }
}

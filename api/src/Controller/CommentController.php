<?php

namespace App\Controller;

use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Comment;
use App\Entity\Post;
use App\Form\CommentType;
use JMS\Serializer\SerializerInterface;
use App\Repository\CommentRepository;
use Symfony\Component\HttpFoundation\JsonResponse;



class CommentController extends AbstractController
{
    /**
     * @var Serializer
     */
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @Route("/comments", methods="GET")
     */
    public function allComments(CommentRepository $commentRepo, Request $request)
    {
        $comments = $commentRepo->findByDesc($request->get('username'));
        $json = $this->serializer->serialize($comments, 'json');

        return new JsonResponse($json, 200, [], true);
    }

    /**
     * @Route("comment/{comment}", methods="GET")
     */
    public function oneComment(Comment $comment)
    {
        return new JsonResponse($this->serializer->serialize($comment, 'json'), 200, [], true);
    }

    /**
     * @Route("add-comment/{post}", methods="POST")
     */
    public function addComment(Post $post, Request $request, ObjectManager $manager)
    {
        $comment = new Comment();

        $form=$this->createForm(CommentType::class,$comment);
        $form->submit(json_decode($request->getContent(), true));
        // $now = new \DateTime();
        if($form->isSubmitted() && $form->isValid()) {
            $comment->setPost($post);
            $comment->setDate(new \DateTime());

            $manager->persist($comment);
            $manager->flush();

            return new JsonResponse($this->serializer->serialize($comment, 'json'), 201, [], true);
        }
        return $this->json($form->getErrors(true), 400);
    }
}

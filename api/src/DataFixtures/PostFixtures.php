<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use App\DataFixtures\CategoryFixtures;
use App\DataFixtures\CommentFixtures;
use App\Entity\Post;
use Doctrine\Persistence\ObjectManager as PersistenceObjectManager;

class PostFixtures extends Fixture implements DependentFixtureInterface
{

    public function load(PersistenceObjectManager $manager)
    {
        // Simone Veil
        $postOne = new Post();
        $postOne->setTitle("Simone Veil : La force de la conviction - Jocelyne Sauvard");
        $postOne->setContent("Je voudrais partager avec vous mon admiration pour Simone Veil.
        Je n’ai vu aucun de ses exploits, que se soit en tant que femme ou en tant que politicienne car l’écart générationnel ne me l’a pas permis.
        Je connaissais son nom parce qu’il le fallait et majoritairement le droit des femmes à l’avortement.
        ⠀
        Cependant, j’ai pu en apprendre davantage sur elle le jour de sa disparition, à travers le petit écran ou dans différents articles.
        La puissance de son histoire ainsi que la force qui émanait d’elle, m’a poussé à lire la biographie que Jocelyne Sauvard a écrit avec brio.
        On y retrouve sa vie, d’Auschwitz au ministère de la santé, jusqu’au Parlement européen.
        ⠀
        Durant ma lecture, j’ai parfois été choqué, ému, décontenancé mais sans cesse admirative.
        À l’instar du journal d’Anne Franck, ces histoires nous poussent à l’introspection et nous font nous poser de nombreuses questions.
        ⠀
        Cette biographie a, je pense, joué son rôle dans mon ascension sociale, professionnelle et personnelle.
        On y apprend que tous les mûrs peuvent être démolis et qu’avec force de conviction, tout est possible.");
        $postOne->setImage('https://zupimages.net/up/20/25/ukz5.jpg');
        $postOne->setAuthor("Mel");
        $postOne->setDate(new \Datetime("now"));
        $postOne->setCategory($this->getReference(CategoryFixtures::CATEGORY_ONE));

        $postOne->addComment($this->getReference(CommentFixtures::COMMENT_TWO));
        $postOne->addComment($this->getReference(CommentFixtures::COMMENT_THREE));

        // Geisha
        $postTwo = new Post();
        $postTwo->setTitle("Geisha - Arthur Golden");
        $postTwo->setContent("Bonjour à tous, ma lecture est terminée et j’ai A-DO-RÉ !
        ⠀
        J’ai appris tellement de choses !
        Notamment la complexité de la vie dans les petits villages du Japon dans les années 30’, obligeant des familles à se séparer de leurs enfants pour leur offrir un avenir dont même les parents n’imaginaient pas la dureté...
        ⠀
        Ce livre efface toutes les idées que nous avons pu nous faire concernant l’existence et le travail d’une Geisha.
        On apprend qu’à cette époque, c’était un véritable enjeu dans la vie d’une Japonaise et que la formation pour le devenir était vraiment difficile.
        ⠀
        J’y ai aussi vu un business. Un status qui avait tout de même pour but de rapporter de l’argent, avant même celui de distraire un homme ou de lui servir du saké.
        Je ne doute pas des rivalités qu’il y avait entre ces femmes 😊.
        ⠀
        Nous avons aussi un passage intéressant sur la seconde guerre mondiale et l’impact qu’elle a eu sur le pays et sa population.
        Que devient une Geisha pendant une guerre de cette envergure ?

        En plus de nous ouvrir à une toute autre culture, Arthur Golden nous fait apprécier le personnage principal, pour qui, on espère une grande réussite 🙂.");
        $postTwo->setImage("https://zupimages.net/up/20/25/4nyu.jpg");
        $postTwo->setAuthor("Mel");
        $postTwo->setDate(new \Datetime("now"));
        // $postTwo->setToLike(true);
        $postTwo->setCategory($this->getReference(CategoryFixtures::CATEGORY_TWO));

        $postTwo->addComment($this->getReference(CommentFixtures::COMMENT_ONE));

        $manager->persist($postOne);
        $manager->persist($postTwo);

        $manager->flush();

    }

    public function getDependencies()
    {
        return array(
            CategoryFixtures::class,
            CommentFixtures::class
        );
    }
}

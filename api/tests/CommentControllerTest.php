<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Client;
use App\DataFixtures\CommentFixtures;
use App\DataFixtures\PostFixtures;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;

class CommentControllerTest extends WebTestCase
{
    /**
     * @var Client
     */
    private $client;

    public function setUp()
    {
        $this->client = static::createClient();
        $manager = $this->client->getContainer()->get('doctrine')->getManager();
        $purger = new ORMPurger($manager);
        $purger->purge();
        $manager->getConnection()->exec("ALTER TABLE comment AUTO_INCREMENT = 1;ALTER TABLE post AUTO_INCREMENT = 1;ALTER TABLE category AUTO_INCREMENT = 1;");
    }

    public function testAllCommentsRoute()
    {
        $this->client->request('GET', '/comments');

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $data = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertSame('Inconnu 1', $data[0]['username']);
    }

    public function testOneCommentRoute()
    {     
        $this->client->request('GET', 'comment/2'); 

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $data = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertSame(2, $data['id']);
        $this->assertSame("Inconnu 2", $data['username']);
    }

    public function testAddComment()
    {
        $this->client->request('POST', '/post/2', [], [], [], json_encode([
            "username" => "Test username",
            "content" => "Test content",
        ]));

        $this->assertSame(201, $this->client->getResponse()->getStatusCode());
        $repo = self::$container->get('App\Repository\CommentRepository');
        $this->assertSame(3, $repo->count([]));
    }
}

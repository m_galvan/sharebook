import Axios from 'axios';

export default class CategoryService {
  constructor() {
    this.url = 'http://localhost:8000/';
  }

  async findAll() {
    const categories = await Axios.get(this.url + 'categories/');
    return categories.data;
  }
  
  async findById(routeParam) {
    const category = await Axios.get(this.url + 'category/' + routeParam);
    category.data.posts.sort().reverse();
    
    return category.data;
  }
}
